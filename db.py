import psycopg2
from sqlalchemy import create_engine
from sqlalchemy import URL
from config import host, user, password, db_name
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import declarative_base
from sqlalchemy import Column, Integer, Float, String, DateTime, VARCHAR


Base = declarative_base()

url = URL.create(
    drivername="postgresql",
    username=user,
    host=host,
    password=password,
    database=db_name,
) 

class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    firstname = Column("firstname", String)
    lastname = Column("lastname", String)
    age = Column("age", Integer)
    # date = DateTime(Column,)

def __init__(self, id, first, last, age):
    self.id = id
    self.firstname = first
    self.lastname = last
    self.age = age

def __repr__(self):
    return f"({self.id}) {self.firstname} {self.lastname} ({self.age})"    

engine = create_engine(url)
Base.metadata.create_all(bind=engine)

Session = sessionmaker(bind=engine)
session = Session()


#session.add()
session.commit()








