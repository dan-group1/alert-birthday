from database.models import User
import psycopg2
from sqlalchemy import create_engine
from sqlalchemy import URL
from config import host, user, password, db_name
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import declarative_base



class DatabaseConnector:
    def __init__(self):
        USERNAME = "agentric"
        connection_string = f"postgresql+psycopg2://{USERNAME}:@localhost:5432/{USERNAME}"
        engine = create_engine(connection_string)
        self.Session = sessionmaker(engine)

    def add(self, title, author, published) -> Book.book_id:
        try:
            session = self.Session()
            today = datetime.now().strftime('%Y-%m-%d')
            book = Book(title = title, author = author, published = published, date_added = today, date_deleted = None)
            session.add(book)
            session.commit()
            return book.book_id
        except Exception as ex: return str(ex)
        finally: session.close()