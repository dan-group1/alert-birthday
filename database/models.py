from sqlalchemy import Column, Integer, Float, String, DateTime, VARCHAR
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    firstname = Column("firstname", String)
    lastname = Column("lastname", String)
    age = Column("age", Integer)