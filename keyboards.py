from aiogram.types import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

kb = ReplyKeyboardMarkup(resize_keyboard=True,
                         one_time_keyboard=True)
b1 = KeyboardButton(text="/help")
b2 = KeyboardButton(text="/description")
kb.add(b1, b2)

ikb = InlineKeyboardMarkup(row_width=2, one_time_keyboard=True)
ib1 = InlineKeyboardButton(text="Да",
                           callback_data='yes')
ib2 = InlineKeyboardButton(text="Нет",
                           callback_data='no')
ib3 = InlineKeyboardButton(text="➡️Next➡️",
                           callback_data='next')
ib4 = InlineKeyboardButton(text="Main menu",
                           callback_data='main')
ikb.add(ib1, ib2)

ikb_class = InlineKeyboardMarkup(row_width=1, resize_keyboard=True, one_time_keyboard=True)
ibc1 = InlineKeyboardButton(text="Классификация1",
                           callback_data='class1')
ibc2 = InlineKeyboardButton(text="Классификация2",
                           callback_data='class2')
ibc3 = InlineKeyboardButton(text="Классификация3",
                           callback_data='class3')
ibc4 = InlineKeyboardButton(text="Классификация4",
                           callback_data='class4')

ikb_class.add(ibc1, ibc2).add(ibc3, ibc4)

HELP_COMMAND = """
/help - список команд
/start - начать работу с ботом
/description
"""