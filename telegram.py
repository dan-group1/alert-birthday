import random
from config import TG_TOKEN
from aiogram.dispatcher.filters import Text
from aiogram.types import ReplyKeyboardRemove
from aiogram import Bot, Dispatcher, types
from aiogram.utils import executor
from keyboards import kb,ikb, HELP_COMMAND, ikb_class
from db import session

bot = Bot(TG_TOKEN)
dp = Dispatcher(bot)

async def on_startup(_):
    print('Бот запущен')

@dp.message_handler(Text(equals="Главное меню"))
async def open_kb(message: types.Message):
    await message.answer(text="Добро пожаловать в главное меню",
                         reply_markup=kb)
    await message.delete()
 
@dp.message_handler(commands=['start'])
async def start_command(message: types.message):
    await message.answer(text="<em>Добро пожаловать в бот уведомления <b>ДР</b> бот</em>",
                         parse_mode="HTML",
                         reply_markup=kb)
    await message.delete()

@dp.message_handler(commands=['help'])
async def help_command(message: types.message):
    await message.answer(text=HELP_COMMAND,
                         parse_mode="HTML")
    await message.delete()

@dp.message_handler(commands=['description'])
async def help_command(message: types.message):
    await message.answer(text="Этот бот создан в образовательных целях")

@dp.message_handler()
async def add_data(message: types.Message):
        await message.answer(message.text)
        name = message.text
        session.add(5,name,'Afonin',25)
        session.commit()


@dp.callback_query_handler()
async def load_desc(callback: types.CallbackQuery):
    if callback.data =='yes':

        await callback.message.answer(text="Выберите тип нарушения",
                                reply_markup=ikb_class)
        # await save_message(data_photo, data_text)
        # await callback.answer('Сохранено')
    else:
        await callback.answer('Нажми кнопку: следующее')




# @dp.callback_query_handler()
# async def callback_random_photo(callback: types.CallbackQuery):
#     global random_photo, flag
#     if callback.data =='save':
#         if not flag:
#             await callback.answer('Сохранено')
#             flag = True
#         else:
#             await callback.answer('Вы уже лайкали')
#         # await callback.message.answer('Вам понравилось')
#     elif callback.data =='dislike':
#         await callback.answer('Вам не понравилось')
#     elif callback.data == 'main':
#         await callback.message.answer(text="Добро пожаловать в главное меню",
#                          reply_markup=kb)
#         await callback.message.delete()
#         await callback.answer()
#     else:
#         random_photo = random.choice(list(filter(lambda x: x!= random_photo, list(photos.keys()))))
#         await callback.message.edit_media(types.InputMedia(media = random_photo,
#                                                            type = 'photo',
#                                                            caption = photos[random_photo]),
#                                                            reply_markup = ikb)
#         # await send_random(message=callback.message)
#         await callback.answer()

